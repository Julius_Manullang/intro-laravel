@extends('master')
@section('content')
<h2>List Pertanyaan</h2>
<div class="mt-3 ml-3">
    <div class="=card">
        <div class="card-header">
            <h3 class="card-title"> </h3>
        </div>
        
<div class="body">
    @if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
@endif


<!-- <a href="/pertanyaan/create" class="btn btn-primary mb-2">Tambah Pertanyaan</a> -->

<a href="{{ route('pertanyaan.create')}}" class="btn btn-primary mb-2">Tambah Pertanyaan</a>

<table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Judul</th>
                <th scope="col">isi</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($pertanyaan as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->judul}}</td>
                        <td>{{$value->isi}}</td>
                        <td>
             
                            <form action="/pertanyaan/{{$value->id}}" method="POST">
                            <a href="/pertanyaan/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            
   <!--              <a href="{{route('pertanyaan.show' , ['pertanyaan => $pertanyaan->id'])}}" class="btn btn-info">Show</a>
                <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-primary">Edit</a> -->


                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection