@extends('master')
@section('content')
<h2>Tambah Jawaban</h2>
        <form action="/jawaban" method="POST">
            @csrf
            <div class="form-group">
                <label for="body">Isi</label>
                <textarea name="isi" id="body" class="form-control" cols="30" rows="30"></textarea>
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
    </div>

@endsection