@extends('master')
@section('content')
<h2>Edit Data</h2>
        <form action="/jawaban/{{$jawaban->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="body">Isi</label>
                <textarea name="isi" id="body" class="form-control" cols="30"  rows="10">{{$jawaban->isi}}
                </textarea>
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>

@endsection