<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Jawaban;

class JawabanController extends Controller
{
   public function create()
    {
        return view('jawaban.create');
    }
     public function store(Request $request)
    {
        $request->validate([
            
            'isi' => 'required',
        ]);
        //Metode Builder

        // $query = DB::table('jawaban')->insert([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"]
        // ]);
        
        //Motode ORM Eloquent (Metode Save)
        // $jawaban = new jawaban;
        // $jawaban->judul = $request["judul"];
        // $jawaban->isi = $request["isi"];
        // $jawaban->save();
 
        //Metode Assignment Eloquent 
        $jawaban = Jawaban::create([
            "isi" => $request["isi"]
        ]);

        return redirect('/jawaban')->with('success' , 'jawaban Berhasil Tersimpan');
    }

    public function index(){
        
        //Metode Builder
        // $jawaban = DB::table('jawaban')->get();

        //Metode ORM Eloquent
        $jawaban = Jawaban::all();

        return view('jawaban.index' , compact('jawaban'));
    }

    public function show($id)
    {
        //Metode Builder
        // $jawaban = DB::table('jawaban')->where('id', $id)->first();

        //Metode Eloquent
        $jawaban = Jawaban::find($id);

        return view('jawaban.show', compact('jawaban'));
    }

    public function edit($id)
    {
        // Metode Builder
        // $jawaban = DB::table('jawaban')->where('id', $id)->first();
        
        // Metode Eloquent
        $jawaban = Jawaban::find($id);

        return view('jawaban.edit', compact('jawaban'));
    }

     public function update($id, Request $request)
    {
        
        // $request->validate([
        //     'judul' => 'required|unique:jawaban',
        //     'isi' => 'required', 
        // ]);

        //Metode Builder
        // $query = DB::table('jawaban')
        //     ->where('id', $id)
        //     ->update([
        //         'judul' => $request["judul"],
        //         'isi' => $request["isi"]
        //     ]);
        // Metode Eloquent
        $update = Jawaban::where('id' , $id)->update([
            "isi" => $request["isi"]
        ]);

        return redirect('/jawaban');
    }
    
    public function destroy($id)
    {
        // Metode Builder
        // $query = DB::table('jawaban')->where('id', $id)->delete();

        // Metode Eloquent
        Jawaban::destroy($id);

        return redirect('/jawaban')->with('success', 'jawaban Berhasil Dihapus');
    }
}
