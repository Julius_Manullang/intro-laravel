<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;


class PertanyaanController extends Controller
{
    public function create()
    {
    	return view('pertanyaan.create');
    }
     public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
        ]);
        //Metode Builder

        // $query = DB::table('pertanyaan')->insert([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"]
        // ]);
        
        //Motode ORM Eloquent (Metode Save)
        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->judul = $request["judul"];
        // $pertanyaan->isi = $request["isi"];
        // $pertanyaan->save();
 
        //Metode Assignment Eloquent 
        $pertanyaan = Pertanyaan::create([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success' , 'Pertanyaan Berhasil Tersimpan');
    }

    public function index(){
    	
        //Metode Builder
        // $pertanyaan = DB::table('pertanyaan')->get();

        //Metode ORM Eloquent
        $pertanyaan = Pertanyaan::all();

    	return view('pertanyaan.index' , compact('pertanyaan'));
    }

    public function show($id)
    {
        //Metode Builder
        // $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();

        //Metode Eloquent
        $pertanyaan = Pertanyaan::find($id);

        return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($id)
    {
        // Metode Builder
        // $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        
        // Metode Eloquent
        $pertanyaan = Pertanyaan::find($id);

        return view('pertanyaan.edit', compact('pertanyaan'));
    }

     public function update($id, Request $request)
    {
        
        // $request->validate([
        //     'judul' => 'required|unique:pertanyaan',
        //     'isi' => 'required', 
        // ]);

        //Metode Builder
        // $query = DB::table('pertanyaan')
        //     ->where('id', $id)
        //     ->update([
        //         'judul' => $request["judul"],
        //         'isi' => $request["isi"]
        //     ]);
        // Metode Eloquent
        $update = Pertanyaan::where('id' , $id)->update([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan');
    }
    
    public function destroy($id)
    {
        // Metode Builder
        // $query = DB::table('pertanyaan')->where('id', $id)->delete();

        // Metode Eloquent
        Pertanyaan::destroy($id);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dihapus');
    }
}
