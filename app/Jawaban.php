<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    protected $table = "jawaban";

    // protected $fillable = ["judul", "isi"];
    protected $guarded = [];
}
