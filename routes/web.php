<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('home');
});

Route::get('/register', 'RegisterController@register');
Route::post('/welcome' , 'RegisterController@post');

Route::get('/', function(){

	return view('welcome');
});

Route::get('data-tables', function()
{
	return view('data-tables');
});



// Route::get('/pertanyaan/create', 'PertanyaanController@create');
// Route::get('/pertanyaan' , 'PertanyaanController@index')->name('pertanyaan.index');
// Route::post('/pertanyaan' , 'PertanyaanController@store');
// Route::get('/pertanyaan/{pertanyaan_id}' , 'PertanyaanController@show');
// Route::get('/pertanyaan/{pertanyaan_id}/edit' , 'PertanyaanController@edit');
// Route::put('/pertanyaan/{pertanyaan_id}' , 'PertanyaanController@update');
// Route::delete('/pertanyaan/{pertanyaan_id}' , 'PertanyaanController@destroy');


//Mengunakan Route Resource
Route::resource('pertanyaan' , 'PertanyaanController');

Route::resource('jawaban' , 'JawabanController');

